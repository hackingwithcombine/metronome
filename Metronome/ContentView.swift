//
//  ContentView.swift
//  Metronome
//
//  Created by Per Friis on 27/01/2021.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var metronomen: Metronome
    
    var body: some View {
        ZStack {
            LinearGradient(gradient: Gradient(colors: [Color(.systemBlue), Color(.systemTeal)]), startPoint: .topLeading, endPoint: .bottomTrailing)
            VStack {
                ZStack {
                    Circle()
                        .fill(Color(.systemPurple))
                        .onTapGesture {
                            print("hello")
                        }
                        .padding()
                    
                    VStack {
                        Text("Beat")
                            .font(.title3)
                        Text("\(metronomen.bpm)")
                            .font(.largeTitle)
                        HStack {
                            if metronomen.currentBeat >= 0 {
                                Text("\(metronomen.currentBeat):\(metronomen.currentTick)")
                            }
                        }
                    }
                }
                if metronomen.isCounting {
                    Button(action: {metronomen.stop()}) {
                        Text("stop")
                    }
                } else {
                    Button(action: {metronomen.start()}) {
                        Text("start")
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(Metronome())
    }
}
