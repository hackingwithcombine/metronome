//
//  Metronome.swift
//  Metronome
//
//  Created by Per Friis on 27/01/2021.
//

import Foundation
import Combine
import AVFoundation

/// The Metronome class is the View-Model/Model of the Metronome app, that serves all the data to the UI, as well as handle the users intend to change values in the model
/// - Tag: metronome
final class Metronome : ObservableObject {
    @Published private(set) var isCounting = false
    @Published private(set) var currentBeat: Int = -1
    @Published private(set) var currentTick: Int = 0
    
    @Published private(set) var bpm: Int = 150
    
    private let click1URL: URL
    private let click2URL: URL
    
    private let click1Player: AVAudioPlayer
    private let click2Player: AVAudioPlayer
    
    private var beatSubscription: AnyCancellable?
    private var subscriptions: [AnyCancellable] = []

    /// Setup the class
    /// - Tag: metronome.init
    init() {
        click1URL = Bundle.main.url(forResource: Self.click1FileName, withExtension: Self.clickExtention)!
        click2URL = Bundle.main.url(forResource: Self.click2FileName, withExtension: Self.clickExtention)!
        
        click1Player = try! AVAudioPlayer(contentsOf: click1URL)
        click1Player.prepareToPlay()
        
        click2Player = try! AVAudioPlayer(contentsOf: click2URL)
        click2Player.prepareToPlay()
        
        /// setting up the subscription of the tick to count the beats and play the sounds
        $currentTick
            .sink { (tick) in
                if tick == 1 {
                    self.currentBeat += 1
                    self.click1Player.play()
                } else if tick <= 4 {
                    self.click2Player.play()
                }
            }
            .store(in: &subscriptions)
    }
    
    
    // MARK: User intends
    /// Start the metronome with the current beet
    /// - Tag: metronome.start
    func start() {
        beatSubscription?.cancel()
        isCounting = true
        beatSubscription = Timer
            .TimerPublisher(interval: bpm.beatInterval, tolerance: 0.001, runLoop: .main, mode: .common)
            .autoconnect()
            .sink(receiveValue: { _ in
                let tick = self.currentTick
                if tick < 4 {
                    self.currentTick = tick + 1
                } else {
                    self.currentTick = 1
                }
            })
    }
    
    /// Cancel the subscription and reset all the values
    /// - Tag: metronome.stop
    func stop() {
        beatSubscription?.cancel()
        isCounting = false
        currentBeat = -1
        currentTick = 0
    }
}


fileprivate extension Int {
    /// just a canculation of the time interval fro the BPM
    var beatInterval: TimeInterval { 60.0 / TimeInterval(self) }
}

/// I like to have the hardcoded values contained
extension Metronome {
    static let click1FileName = "metronom1"
    static let click2FileName = "metronom2"
    static let clickExtention = "aif"
}
