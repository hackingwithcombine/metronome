//
//  MetronomeApp.swift
//  Metronome
//
//  Created by Per Friis on 27/01/2021.
//

import SwiftUI

@main
struct MetronomeApp: App {
    @StateObject private var metronome = Metronome()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(metronome)
        }
    }
}
