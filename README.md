#Combine - TimerPublisher
The TimerPublisher is a publisher on the current timer, and can be used in much the same way as you use the scheduled timer, only that the patten has changed to use combine.  
The timerPublisher is special as is a __Connectable publisher__, that you have to start / connect for it to start publish.  
In the examples I have here, I use auto connect, enable us to use the publisher, immediately, as we would any other publisher.  

The timer publisher is by default repeating the call, and you have to cancel you subscription, when you want it to stop. 

## The App.  
I have create an Metronome app, just as the metronome is an example of a timer, that runs rapidly, so we have some some quick feedback on our changes.

### SwiftUI
This is not a swiftUI sample/tutorial, for that please go to the [hacking with swift](https://hackingwithswift.com) site, there is a lot of great swiftUI samples.  
 All I have to say for the SwiftUI, is the cool way of using bindings/state, you should check that out, as this is often the glue between __Combine__ and __SwiftUI__!  
 
### View Model, Model
The Model of this app is just a couple of values, so I won't make a separate model for this.  
Remember the standard architecture of SwiftUI app are MVVM, and I always encourage to use the original/intended architecture, of the API, otherwise you are shooting your self in the fool, just my opinion!  
  
So the view-model of this app, is the [Metronome class](x-source-tag://metronome).  

It holds all the relevant data, the model, and publish the values, as the metronome progress and plays the sounds.

the click,URL's and players, have as such no special connection to the combine, but just an enabler for us to have a click when the metronome is clicking away

#Metronome. 
In the [```init()```](x-source-tag://metronome.init) set-up the sounds and create a subscription on the ticks.  
The tick subscription is the one that plays the sound and update the values for the ```currentBeat``` this publishers is also used by the swiftUI to update the view.

The [```func start()```](x-source-tag://metronome.start) starts the metronome by subscribing to a timer, and let the subscriber know when a tick have been published.  
When I have a names dedicated subscriber to a publisher, I tends to start by cancel the subscription if it is there any publisher running, and when you have an optional subscription, it is safe just to call the ```cancel()```.  
In this case, I need a way to say that the metronome is running, here I use a low-tech solution, with a boolean.  
Now we come to the publisher.  
The publisher is an extension to the Timer class, and works a lot like the scheduled timer function.
The publisher is also a __connectable publisher__ meaning, that you have to "connect"/start the publisher before it starts publishing. but you can add the ```.autoconnect()```modifier on the publisher to start the connection right away.  
The ```TimePublisher``` parameters varies, I have used the the mandatory fields, ```interval:, runLoop: and mode:``` and I have also added the ```tolerance:``` in this case as I like the metronome to have some precision, as we don't want the beat all over the place.  
the __subscriber__ ```sink``` only have the ```receiveValue``` as the ```TimerPublisher``` never fails and we just get the time.  
The publisher also never ends, unless it is cancelled, so it just keep going, as we need it too.
In the ```receivedValue``` is called each 60/bpm sec. then I count up the beat, setting another chain in motion.  

the [```func stop()```](x-source-tag://metronome.stop) are just cancelling the subscription, and resetting the values. and ... the metronome stops.

---
This is a typically patten for my viewModel by having some internal subscriptions that are set-up in the ```init()``` or a function called from the ```init()``` and then I have the users intend after if there are user interactions, with the view-model, which often is the case.
